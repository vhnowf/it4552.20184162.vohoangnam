<html>
<head>
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Date Time Processing</title>
    
</head>
<body>
<?php 
    $result = '';
    if(isset($_POST['submit'])){ 
        if (empty($_POST['name']) || empty($_POST['date'])){
            $result = "Please fill all information!";
        } else {
            $name = isset($_POST['name']) ? $_POST['name'] : '';
            $date = isset($_POST['date']) ?  date('d/m/Y', strtotime($_POST['date'])) : '';
            $days_of_month =  date('t', strtotime($_POST['date']));
            $hour = isset($_POST['input-field-hour']) ? $_POST['input-field-hour'] : '';
            $minute = isset($_POST['input-field-minute']) ? $_POST['input-field-minute'] : '';
            $second = isset($_POST['input-field-second']) ? $_POST['input-field-second'] : '';
            $result = "Hi " . $name . "!<br> You have an apointment on " . $hour . ":" . $minute . ":" . $second . ", " 
            . $date . "<br><br> More information <br><br> In 12 hours, the time and date is ";
            if ($hour > 12 ){
                $hour_12 = $hour - 12;
                $result .= $hour_12 . ":" . $minute . ":" . $second . " PM, " . $date;
            } else $result .= $hour . ":" . $minute . ":" . $second . " AM, " . $date;
            $result .= "<br><br> This month has " . $days_of_month . " days";
        }
    }

?>
<div class="container">
    <div class="describe-text">
        Enter your name and select date and time for the appointment
    </div>
    <form method="post">
        <div class="input-field">
            <label name = "input-field-name">Your name: </label>
            <input type = "text" name = "name" value = <?php echo isset($name) ? $name : ''?>>
        </div>
        <div class="input-field">
            <label name = "input-field-name">Date: </label>
            <input type = "date" name = "date" value = "<?php echo !empty($_POST['date']) ? date('Y-m-d', strtotime($_POST['date'])) : ''?>" >
        </div>
        <div class="input-field">
            <label name = "input-field-name">Time: </label>
            <select name = "input-field-hour">
                <?php 
                    echo isset($hour) ?  '<option value='.$hour.'>'.$hour.'</option>' : '';
                    $hours = range(0, 23);
                        foreach ($hours as $h) {
                            echo '<option value='.$h.'>'.$h.'</option>';
                        } 
                ?> 
                <?php 
                    
                ?> 
            </select>
            <select name = "input-field-minute">
                <?php 
                    echo isset($minute) ?  '<option value='.$minute.'>'.$minute.'</option>' : '';
                    $minutes = range(0, 59);
                        foreach ($minutes as $m) {
                            echo '<option value='.$m.'>'.$m.'</option>';
                        } 
                ?> 
            </select>
            <select name = "input-field-second">
                <?php
                    echo isset($second) ?  '<option value='.$second.'>'.$second.'</option>' : '';
                    $seconds = range(0, 59);
                        foreach ($seconds as $s) {
                            echo '<option value='.$s.'>'.$s.'</option>';
                        } 
                ?> 
            </select>
        </div>
        <input type="submit" name="submit" value="Submit" >
    </form>
    <?php echo $result?>
</div>

</body>
</html>