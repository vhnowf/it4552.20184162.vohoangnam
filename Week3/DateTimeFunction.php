<html>
<head>
    <meta charset="utf-8">  
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">  
    <title>Date Time Function</title>
    
</head>
<body>
<?php 
    $result = "";

    // function to validate birthday
    function ValidateBirthday($date) {
        $today = strtotime(date("Y/m/d"));
        $diff = $today - strtotime($date);
        if ($diff < 0) {
            return 0;
        } 
        return 1;
    }
    if(isset($_POST['submit'])){ 
        if (empty($_POST['name-1']) || empty($_POST['name-2']) || empty($_POST['date-1'])|| empty($_POST['date-2'])){
            $result = "Please fill all information!";
        } else if (ValidateBirthday($_POST['date-1']) == 0 || ValidateBirthday($_POST['date-2'])== 0){
            $result = "Birthday invalid!";
        } else {
            $name1 =  isset($_POST['name-1']) ? $_POST['name-1'] : '';
            $name2 =  isset($_POST['name-2']) ? $_POST['name-2'] : '';
            $dob1  =  isset($_POST['date-1']) ? $_POST['date-1'] : '';
            $dob2  =  isset($_POST['date-2']) ? $_POST['date-2'] : '';
            $diff_days = caculateDifferentDays($dob1, $dob2);
            $age1 = calculateAge($dob1);
            $age2 = calculateAge($dob2);
            $diff_years = caculateDifferentYears($dob1, $dob2);
            $dob1_display = displayBirthday($dob1);
            $dob2_display = displayBirthday($dob2);
        } 
    }

    // function to calculate age
    function calculateAge($date){
        $today = strtotime(date("Y/m/d"));
        $diff = $today - strtotime($date);
        $age = floor($diff / (365*60*60*24));   
        return $age;
    }

    // function to calculate different days
    function caculateDifferentDays($date1,$date2){
        $first_date = strtotime($date1);
        $second_date = strtotime($date2);
        $diff = abs($first_date - $second_date);
        $days = floor(($diff / (60*60*24)));
        return  $days;
    }

     // function to calculate different years
    function caculateDifferentYears($date1,$date2){
        $first_date = strtotime($date1);
        $second_date = strtotime($date2);
        $diff = abs($first_date - $second_date);
        $years = floor(($diff / (365*60*60*24)));
        return  $years;
    }

     // function to calculate display birthday
    function displayBirthday($date){
        $d =  date('d', strtotime($date));
        $m =  date('m', strtotime($date));
        $y=  date('Y', strtotime($date));
        $info = date('l', mktime(0, 0, 0, $m, $d, $y));
        $result = $info . ", " ;
        switch ($m){
            case 1:
                $result .= "January ";
                break;
            case 2:
                $result .= "February ";
                break;
            case 3:
                $result .= "March ";
                break; 
            case 4:
                $result .= "April ";
                break;
            case 5:
                $result .= "May ";
                break;
            case 6:
                $result .= "June ";
                break;  
            case 7:
                $result .= "July ";
                break;
            case 8:
                $result .= "August ";
                break;
            case 9:
                $result .= "September ";
                break;   
            case 10:
                $result .= "October ";
                break;
            case 11:
                $result .= "November ";
                break;
            case 12:
                $result .= "December ";
                break;  
        }
        $result .=  $d . ", " . $y;
        return $result;
    }
?>
<div class="container">
    <div class="describe-text">
        Enter name and select birthday
    </div>
    <form method="post">
        <div class="input-field">
            <label name = "input-field-name">First name: </label>
            <input type = "text" name = "name-1"> 
        </div>
        <div class="input-field">
            <label name = "input-field-name">First birthday: </label>
            <input type = "date" name = "date-1">
        </div>
        <div class="input-field">
            <label name = "input-field-name">Second name: </label>
            <input type = "text" name = "name-2"> 
        </div>
        <div class="input-field">
            <label name = "input-field-name">Second birthday: </label>
            <input type = "date" name = "date-2">
        </div>
        <input type="submit" name="submit" value="Submit" >
    </form>
    <?php   
            echo $result;
        if(isset($_POST['submit']) && empty($result)){
            echo $name1 . "'s bithday is " .$dob1_display . "<br>";
            echo $name2 . "'s bithday is " .$dob2_display . "<br>";  
            echo "The difference in days between two dates is " . $diff_days . " day(s) <br>";      
            echo $name1 . " is " . $age1 . " years old <br>"; 
            echo $name2 . " is " . $age2 . " years old <br>"; 
            echo "The difference in years between two dates is " . $diff_years . " year(s) <br>";      
        }
    ?>
</div>

</body>
</html>