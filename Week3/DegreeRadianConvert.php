<html>
<head>
    <meta charset="utf-8">  
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">  
    <title>Degree Radian Function</title>
    
</head>
<body>
<?php 
    $degree_res = "";
    $radian_res = "";
    $type = "";
    // function to convert radian to degree and vice versa
    function Convert($mode, $value) {
        $res = 0;
        if($mode == 0){
            $pi = 3.14;
            $res = ($value * 180 / $pi);
        }
        else {
            $pi = 3.14;
            $res = ($value * $pi / 180);
        }
        return $res;
    }

    if(isset($_POST['submit-1'])){ 
        if (empty($_POST['degree-1'])){
            $result1 = "Please enter degree value!";
        } else {
            $type = 1;
            $degree = $_POST['degree-1'];
            $radian_res =  Convert($type, $degree);
        } 
    }
    if(isset($_POST['submit-2'])){ 
        if (empty($_POST['radian-1'])){
            $result2 = "Please enter radian value!";
        } else {
            $type = 0;
            $radian = $_POST['radian-1'];
            $degree_res =  Convert($type, $radian);
        } 
    }
?>
<div class="container">
    <div class="describe-text">
        <h2>Convert degree to radian</h2>
    </div>
    <form method="post">
        <div class="input-field">
            <label name = "input-field-name">Degree value: </label>
            <input type = "text" name = "degree-1" value =  <?php echo isset($degree) ? $degree : "" ?>> 
        </div>
        <br>
        <input type="submit" name="submit-1" value="Convert degree to radian" >
        <br>
        <br>
        <div class="input-field">
            <label name = "input-field-name">Radian value: </label>
            <input type = "text" value = <?php echo isset($_POST['submit-1']) ? $radian_res : "" ?>>
        </div>
        <?php echo isset($result1) ? $result1 : "" ?>
    </form>
    <div class="describe-text">
        <h2>Convert radian to degree </h2> 
    </div>
    <form method="post">
        <div class="input-field">
            <label name = "input-field-name">Radian value: </label>
            <input type = "text" name = "radian-1" value = <?php echo isset($radian) ? $radian : "" ?>> 
        </div>
        <br>
        <input type="submit" name="submit-2" value="Convert radian to degree" >
        <br>
        <br>
        <div class="input-field">
            <label name = "input-field-name">Degree value: </label>
            <input type = "text" value = <?php echo isset($_POST['submit-2']) ? $degree_res : "" ?>>
        </div>
        <?php echo isset($result2) ? $result2 : "" ?>
    </form>
</div>

</body>
</html>