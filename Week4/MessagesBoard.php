<html>
    <head><title>Messages Board</title></head>
    <body>
    <?php 
        $error = '';
        if(isset($_POST['submit'])){
            if((empty($_POST['author']) && empty($_POST['anonymous'])) || trim($_POST['msg-content']) ==""){
                $error =  "Lỗi chưa nhập đủ thông tin";
            }else {
            // save file upload to folder
            if (isset($_FILES['image'])){
                    $path = './posts/'.'post_'.$_SERVER['REQUEST_TIME'].'.'.pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['image']['tmp_name'], $path);
            }
            if(isset($_POST['anonymous'])){
                $data = "Anonymous says ". $_POST["msg-content"];
            }else { 
                $data = $_POST["author"] ." says ". $_POST["msg-content"];
            }
            // save message into file 
            $fp = @fopen('./posts/'.'post_'.$_SERVER['REQUEST_TIME'].'.txt',"w");
            fwrite($fp, $data);
            }
        
        }
    ?>
        <h1>Messages Board</h1>
        <?php echo isset($error) ? $error : ''?>
        <div class="show-mess-box">
            <?php
                $path = './posts';
                $file_name = scandir($path);
                $len = count($file_name);
                for ($i = 0; $i < $len; $i++) {
                    if($i >= 2){
                        print "<div class = \"mess-box\" style=\"border:thin solid black; background:#FCEDDD;margin-bottom:10px;\">";
                        if (strpos($file_name[$i],'.PNG') || strpos($file_name[$i],'.JPG') 
                        || strpos($file_name[$i],'.JPEG')|| strpos($file_name[$i],'.png') 
                        || strpos($file_name[$i],'.jpg') || strpos($file_name[$i],'.jpeg')) {
                            $file_path = $path . '/' . $file_name[$i];
                            print "<div class = \"img-block\" style=\"display=inline-block\">";
                            print "<img src='$file_path' width=25%> "; 
                            print "</div>";
                            print "<div class = \"msg-block\" style=\"display=inline-block\">";
                            $file_path = $path . '/' . $file_name[$i+1];
                            $fp = @fopen($file_path, "r");
                            while(!feof($fp)){
                                print fgets($fp);
                            }
                            print "</div>";
                            $i++;
                        }else if(strpos($file_name[$i],'.txt') !== false){
                            $file_path = $path . '/' . $file_name[$i];
                            print "<div class = \"msg-block\" style=\"display=inline-block\">";
                            $fp = @fopen($file_path, "r");
                            while(!feof($fp)){
                                print fgets($fp);
                            }
                            print "</div>";
                        }
                        echo '<br>';
                        print "</div>"; 
                    }
                }
            ?>
        </div>
        <br>
        <div class = "upload-msg-box">
            <form method="post" action = "MessagesBoard.php" enctype="multipart/form-data">
                Tên người dùng: 
                <input type="text" name="author">
                <br>
                <input type="checkbox" name="anonymous"> Đăng thông điệp vô danh
                <br>
                Nội dung thông điệp: 
                <br>
                <textarea name="msg-content" rows="5" cols="30">
                </textarea>
                <br>
                Hình ảnh kèm theo (tuỳ chọn): 
                <br>
                <input type="file" name="image">
                <br>
                <input type="submit" value="Đăng thông điệp" name="submit">
            </form>
        </div>
    </body>
</html>