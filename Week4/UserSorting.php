<?php
    function user_sort($a, $b) {
    // smarts is all-important, so sort it first
        if($b == 'smarts') {
            return 1; 
        }else if($a == 'smarts') {
            return -1;
        }
        return ($a == $b) ? 0 : (($a < $b) ? -1 : 1);
    }
    $values = array('name' => 'Buzz Lightyear',
                    'email_address' => 'buzz@starcommand.gal',
                    'age' => 32,
                    'smarts' => 'some');
    $submitted = "";
    $sort_type = "";
    if(isset($_POST['submitted'])){ 
        $submitted = $_POST['submitted'];
        $sort_type = $_POST['sort_type'];
        if($sort_type == 'usort' || $sort_type == 'uksort' || $sort_type == 'uasort'){ 
            $sort_type($values, 'user_sort'); 
        }else { 
            $sort_type($values); 
        } 
    } 
?>
<form action="UserSorting.php" method="POST">
        <table>
        <tr>
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="sort" /> Standard sort
            </td>
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="usort" />   User-defined sort
            </td >
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="krsort" />   Reverse key sort
            </td >
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="asort" />  Value sort
            </td>
        </tr>
        <tr>
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="rsort" />  Reverse sort 
            </td>
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="ksort" />   Key sort
            </td>
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="uksort" />  User-defined key sort
            </td>
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="arsort" /> Reverse value sort 
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td style="padding-right:100px">
                <input type="radio" name="sort_type" value="uasort"  /> User-defined value sort 
            </td>
        </tr>
        </table>
        <br>
        <input type="submit" value="Sort" name="submitted" style="margin-left:250px" />
        <br>
        <br>
        <table>   
            <tr>
                <td style="padding-right:50px">
                    <p>
                        Values unsorted (before sort):
                    </p>
                </td>
                <td stype="padding-right:50px">
                    <ul> 
                    <?php
                        $v_arr = array('name' => 'Buzz Lightyear',
                                        'email_address' => 'buzz@starcommand.gal',
                                        'age' => 32,
                                        'smarts' => 'some');
                        foreach($v_arr as $key=>$value) {
                            echo "<li><b>$key</b>: $value</li>";
                        } 
                    ?>
                    </ul>
                </td>
                <td style="padding-right:50px">
                    <p> 
                        <?php
                        if(!empty($submitted)){
                            print "Values " . "sorted by $sort_type";
                        }
                        ?>
                    </p>
                </td>
                <td stype="padding-right:50px">
                    <ul> 
                    <?php
                        if(!empty($submitted)){
                            foreach($values as $key=>$value) {
                                echo "<li><b>$key</b>: $value</li>";
                            } 
                        }
                    ?>
                    </ul>
                </td>
            </tr>  
        </table>
        
</form>