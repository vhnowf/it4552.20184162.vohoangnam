<?php
session_start();
require_once("config.php");

$checkout_msg = 'hidden';
$sql = "SELECT * FROM products";
$result = mysqli_query($db, $sql);
if (!$result) {
    $error = "Occur error";
}

if (isset($_GET['remove']) && (!empty($_GET['remove'] || $_GET['remove'] == 0))) {
    unset($_SESSION['cart'][$_GET['remove']]);
}

if (isset($_POST['submit']) && count($_SESSION["cart"])) {
    $_SESSION['cart'] = array();
    unset($_POST['submit']);
    $checkout_msg = 'visible';
}
?>

<html>

<head>
    <title>View shopping cart</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="wrapper">
        <div class="search-box-wrapper">
            <form action="product_details.php" method="get">
                <select name="productID" id="" class="search-input">
                    <option value="" disabled selected>--Search for a product--</option>
                    <?php
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            print "<option value=" . $row['ID'] . " >" . $row['Name'] . "</option>";
                        }
                    }
                    ?>
                </select>
                <input type="submit" value="Search" class="search-btn">
            </form>
        </div>

        <h1>View shopping cart</h1>
        <hr>

        <div class="cart-info">
            <form action="" method="post">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="software-title">Software Title</th>
                            <th>Deliverable</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $product_in_cart = $_SESSION["cart"];
                        $total_price = 0;
                        $qty = count($product_in_cart);

                        if ($qty == 0) {
                            echo '<td colspan="4">Your cart is empty</td>';
                        } else {
                            foreach ($product_in_cart as $key => $value) {
                                $total_price += $product_in_cart[$key]["price"];
                        ?>
                                <tr>
                                    <td class="img-wrapper">
                                        <img src="<?php echo $product_in_cart[$key]["image"]; ?>" alt="">
                                    </td>
                                    <td>
                                        <p><?php echo $product_in_cart[$key]["name"]; ?></p>
                                        <a href="" style="margin-right: 10px;">Edit</a>
                                        <a href="?remove=<?php echo $key; ?>">Remove</a>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $product_in_cart[$key]["deliverable"]; ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php echo $product_in_cart[$key]["price"]; ?>$
                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                        <tr>
                            <td colspan="4">Total: <?php echo $total_price; ?>$</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="btn-wrapper" style="text-align: left;">
                                <input type=button onClick="location.href='welcome.php'" value='Continue Shopping'>
                            </td>
                            <td colspan="2" class="btn-wrapper" style="text-align: right;">
                                <input type="submit" value="Check out" name="submit">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="checkout-msg-wrapper" style="visibility:<?php echo $checkout_msg ?>;">
            <h1>Checkout Success</h1>
            <h3>Thank you for your purchase!</h3>
        </div>
    </div>
</body>

</html>