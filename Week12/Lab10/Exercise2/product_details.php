<?php
session_start();
require_once("config.php");

$msg = "";

$sql1 = "SELECT ID, Name FROM Products";
$result1 = mysqli_query($db, $sql1);

if (!$result1) {
    $error = "Error occur";
}
$row2 = "";
if(!empty($_GET['productID'])){
    $productID = $_GET['productID'];
    $sql2 = "SELECT * FROM Products WHERE ID = $productID";
    $result2 = mysqli_query($db, $sql2);
    if (!$result2) {
        $error = "Error occur";
    }else {
        $row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC);
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['deliverable'])) {
        $deliverable = 'Download';
    } else {
        $deliverable = 'Not download';
    }

    $product = array('productID' => $row2["ID"],
                    'name' => $row2["Name"],
                    'price' => $row2["Price"],
                    'image' => $row2["Image"],
                    'deliverable' => $deliverable);
    array_push($_SESSION["cart"], $product);

    $msg = 'Add product to cart<br>';
}
?>

<html>

<head>
    <title><?php echo $row2['Name']; ?></title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="wrapper">
        <div class="search-box-wrapper">
            <form action="product_details.php" method="get">
                <select name="productID" class="search-input">
                    <option value="" disabled selected>--Search for a product--</option>
                    <?php
                    if (mysqli_num_rows($result1) > 0) {
                        while ($row1 = mysqli_fetch_array($result1)) {
                            print "<option value=" . $row1['ID'] . " >" . $row1['Name'] . "</option>";
                        }
                    }
                    ?>
                </select>
                <input type="submit" value="Search" class="search-btn">
            </form>
        </div>

        <div class="product-details">
            <h1>Product Details</h1>
            <div class="view-cart-btn-wrapper">
                <input type=button onClick="location.href='view_shopping_cart.php'" value='View cart'>
            </div>

            <hr>

            <div class="nmsg-wrapper">
                <?php
                echo $msg;
                $msg = "";
                ?>
            </div>

            <table class="product-details-table">
                <tr>
                    <th>Product name</th>
                    <td><?php echo $row2 ? $row2["Name"] : ""; ?></td>
                    <td class="img-wrapper" rowspan="5">
                        <img src="<?php echo $row2 ? $row2["Image"]: ""; ?>" alt="">
                        <br>
                        <a href="">View Product Description</a>
                    </td>
                </tr>
                <tr>
                    <th>Publisher</th>
                    <td><a href="<?php echo  $row2 ? $row2["PublisherURL"] : ""; ?>">
                    <?php echo  $row2 ? $row2["Publisher"] : ""; ?></a></td>
                </tr>
                <tr>
                    <th>SKU</th>
                    <td><?php echo  $row2 ? $row2["SKU"] : ""; ?></td>
                </tr>
                <tr>
                    <th>Platform</th>
                    <td><?php echo $row2 ? $row2["Platform"] : ""; ?></td>
                </tr>
            </table>
        </div>

        <div class="deliver-and-price">
            <form action="" method="post">
                <table class="deliver-and-price-table">
                    <tr>
                        <td></td>
                        <th>Deliverable</th>
                        <th>Description</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td><input type="radio" name="deliverable" id=""></td>
                        <th>Download</th>
                        <td>Choose this option if you wish to download the software over the Internet.</td>
                        <td><?php echo $row2 ? $row2['Price'] : ""; ?>$</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="add-to-cart-wrapper">
                            <input type="submit" value="Add to cart" class="add-to-cart-btn" onclick="addProductToCart($row2, $productID)">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

</body>

</html>