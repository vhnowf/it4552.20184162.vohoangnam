<?php
session_start();
require_once("config.php");

$sql = "SELECT * FROM products";
$result = mysqli_query($db, $sql);

if (!$result) {
    $error = "sth is wrong";
}

unset($_SESSION["cart"]);

if (!isset($_SESSION["cart"])) {
    $_SESSION["cart"] = array();
}
?>

<html>

<head>
    <title>The store</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="wrapper">
        <h1>Hello, welcome</h1>

        <div class="search-box-wrapper">
            <form action="product_details.php" method="get">
                <select name="productID" id="" class="search-input">
                    <option value="" disabled selected>--Search for a product--</option>
                    <?php
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            print "<option value=" . $row['ID'] . " >" . $row['Name'] . "</option>";
                        }
                    }
                    ?>
                </select>
                <input type="submit" value="Search" class="search-btn">
            </form>
        </div>

        <div class="product-list">
            
        </div>
    </div>
</body>

</html>