CREATE TABLE IF NOT EXISTS Products (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(50),
    Price FLOAT,
    Publisher VARCHAR(50),
    PublisherURL VARCHAR(100),
    SKU VARCHAR(50),
    Platform VARCHAR(50),
    Image VARCHAR(50)
    );


INSERT INTO `Products`(`ID`, `Name`, `Price`, `Publisher`, `PublisherURL`, `SKU`, `Platform`, `Image`) VALUES
(1,'Access 2003',49,'Microsoft','https://www.microsoft.com/','0000','Windows','images/1.jpg'),
(2,'Office 365',99,'Microsoft','https://www.microsoft.com/','0000','Windows','images/2.png'),
(3,'SQL Serve 2008 Edition',29,'Microsoft','https://www.microsoft.com/','0000','Windows','images/3.png');
