<html>
<head>
    <meta charset="utf-8">
    <title>Function</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="ex2.css">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
</head>
<body>
<?php 
    $result = "";
    $txtinput = "";
    $func = "";
    if(isset($_POST['submit'])){ 
        if (empty($_POST['txtinput']) || empty($_POST['func'])){
            $result = "Please fill all boxes and choose a function to submit!";
        } else {
            $txtinput = $_POST['txtinput'];
            $func = $_POST['func'];
            switch($func)
            {
                case 'strlen':
                    $result = strlen($txtinput);
                break;

                case 'tolower':
                    $result = strtolower($txtinput);
                break;

                case 'trim':
                    $result =  trim($txtinput);
                break;

                case 'toupper':
                    $result =  strtoupper($txtinput);
                break;
            } 
        }
    }
?>
<div class="container">
    <form method="post">
        <input type="text" name="txtinput" placeholder="Nhập nội dung xâu ký tự" value = "<?php echo $txtinput; ?>">
        <br>
        <table> 
            <tr>
                <td>
                    <input type="radio" name="func" value="strlen"  <?php echo ($func == 'strlen') ? "checked": ""?>>
                    <label> strlen </label>
                </td>
                <td>
                    <input type="radio" name="func" value="tolower"  <?php echo ($func == 'tolower') ? "checked": ""?>>
                    <label> strtolower </label>
                </td>
            </tr>
            <tr>
            <td>
                <input type="radio" name="func" value="trim"  <?php echo ($func == 'trim') ? "checked": ""?>>
                    <label> trim </label>
                </td>
                <td>
                    <input type="radio" name="func" value="toupper"  <?php echo ($func == 'toupper') ? "checked": ""?>>
                    <label> strtoupper </label>
                </td>
            </tr>
        </table>
        <input type="submit" name="submit" value="Submit" class="btn btn-primary btn-block">

        Result:</label> <?php echo $result; ?>
    </form>
</div>
</body>
</html>