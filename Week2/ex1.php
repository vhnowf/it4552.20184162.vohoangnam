<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="ex1.css">
    <title>Calculator</title>
    
</head>
<body>
<?php 
    $result = "";
    $a = "";
    $b = "";
    $op = "";
    if(isset($_POST['submit'])){ 
        $result = "";
        if (empty($_POST['num1']) || empty($_POST['num2']) || empty($_POST['op'])){
            $result = "Please fill all boxes and choose an operator!";
        } else {
            $a = $_POST['num1'];
            $b = $_POST['num2'];
            $op = $_POST['op'];
            switch($op)
            {
                case '+':
                    $result =  $a + $b;
                break;

                case '-':
                    $result =  $a - $b;
                break;

                case '*':
                    $result =  $a * $b;
                break;

                case '/':
                    $result =  $a / $b;
                break;
            } 
        }
    }
?>
<div class="container">
    <form method="post">
        <input type="text" name="num1" placeholder="Số hạng thứ 1" value = "<?php echo $a; ?>">
        <br>
            <input type="radio" name="op" value="+"  <?php echo ($op == '+') ? "checked": ""?>>
            <label> + </label>
            <input type="radio" name="op" value="-"  <?php echo ($op == '-') ? "checked" : ""?>>
            <label> - </label>
            <input type="radio" name="op" value="*"  <?php echo ($op == '*') ? "checked" : ""?>>
            <label> * </label>
            <input type="radio" name="op" value="/"  <?php echo ($op == '/') ? "checked" : ""?>>
            <label> / </label>
        <br>
        <input type="text" name="num2" placeholder="Số hạng thứ 2" value = "<?php echo $b; ?>">
        <br>
        <br>
        <input type="submit" name="submit" value="Submit" class="btn btn-primary btn-block">

        Result:</label> <?php echo $result; ?>
    </form>
</div>

</body>
</html>