<html>
<head>
    <title>Create Table</title>
</head>
<body>
    <?php
    $server = 'localhost';
    $user = 'root';
    $pass = '123';
    $mydb = 'business_service';
    $table_name1 = 'Businesses';
    $table_name2 = 'Categories';
    $table_name3 = 'Biz_Categories';
    
    $connect = new mysqli($server, $user, $pass);
    if (!$connect) {
        die ("Cannot connect to $server using $user");
    } else {
        $SQLcmd1 = "CREATE DATABASE $mydb";
        if (!$connect->query($SQLcmd1) ){
            die ("Database Create Creation Failed");
        }else {

        $link = mysqli_connect($server, $user, $pass, $mydb);  
        
        $SQLcmd2 = "CREATE TABLE $table_name1 (
            BusinessID INT AUTO_INCREMENT PRIMARY KEY,
            Name VARCHAR(50) NOT NULL,
            Address VARCHAR(50) NOT NULL,
            City VARCHAR(50) NOT NULL,
            Telephone INT NOT NULL,
            URL VARCHAR(50))";
        
        $SQLcmd3 = "CREATE TABLE $table_name2 (
            CategoryID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            Title VARCHAR(50) NOT NULL,
            Description VARCHAR(50) NOT NULL)";

        $SQLcmd4 = "CREATE TABLE $table_name3 (
            BusinessID INT NOT NULL ,
            CategoryID INT NOT NULL)";
            //  PRIMARY KEY(BusinessID,CategoryID)";

        $SQLcmd5 = "ALTER TABLE `$table_name3`
        ADD KEY `bidfk` (`BusinessID`),
        ADD KEY `cidfk` (`CategoryID`)";

        $SQLcmd6 = "ALTER TABLE `Biz_Categories`
        ADD CONSTRAINT `bidfk` FOREIGN KEY (`BusinessID`) REFERENCES `Businesses` (`BusinessID`),
        ADD CONSTRAINT `cidfk` FOREIGN KEY (`CategoryID`) REFERENCES `Categories` (`CategoryID`)";
        if (!mysqli_query($link,$SQLcmd2) || !mysqli_query($link,$SQLcmd3) ){
            die ("Table Create Creation Failed");
        }else {
            if (!mysqli_query($link,$SQLcmd4)){
                die ("Table Create Creation Failed 1");
            }else{
                mysqli_query($link,$SQLcmd5);
                mysqli_query($link,$SQLcmd6);
                print("Create tables and relationships for business listing service");
        }
            }
            
            mysqli_close($link);
        }
    }
    ?>
</body></html>