<html>
    <head><title>Business Listings</title></head>
<body>
    <?php
        $server = 'localhost';
        $user = 'root';
        $pass = '123';
        $table_name1 = 'Businesses';
        $table_name2 = 'Biz_Categories';
        $table_name3 = 'Categories';
        $database = 'business_service';
        $results_id_categories = '';
        $results = '';
        $i = 0;
        $cat_id = '';
        $connect = mysqli_connect($server, $user, $pass);
        if (!$connect) {
            die ("Cannot connect to $server using $user");
        } else {
            if(isset($_GET['cat_id'])){
                $cat_id = $_GET['cat_id'];
            }
                mysqli_select_db($connect, $database);
                $select_query1 = "SELECT * FROM $table_name3";
                $results_id_categories = mysqli_query($connect,$select_query1);
                $sql = "SELECT * FROM Businesses b, Biz_Categories bc WHERE";
                $sql .= " CategoryId = '$cat_id'";
                $sql .= " and b.BusinessId = bc.BusinessId";
                $results = mysqli_query($connect,$sql);
              
        }
    ?>
    <h1>Business Listings</h1>
    <table>
        <tr>
            <td>
                <form  method="post">
                    <table border="3">
                    <tr>
                        <th>Click on a category to find business listings:</th>
                    </tr>
                    <?php
                    if ($results_id_categories) {
                        $PHP_SELF = $_SERVER["PHP_SELF"];
                        while ($row = mysqli_fetch_array($results_id_categories,MYSQLI_NUM)){
                            print '<tr><td class="formlabel">';
                            print "<a href=\"$PHP_SELF?cat_id=$row[0]\" >"; echo "$row[1]</a></td></tr>\n";
                        }
                    }
                    ?>
                    </table>
                </form>
            </td>
            <td valign="top">
            <table border=1>
                <?php
                    if ($results){
                    if($results->num_rows != 0){
                        while ($row = mysqli_fetch_row($results)){
                            print '<tr>';
                            foreach ($row as $field) {
                                print "<td>$field</td>";
                            }
                            print '</tr>';
                        }
                    }else {
                        print '<tr>';
                        print "<td>This category has no business</td>";
                        print '</tr>';
                    }
                }
                ?>
            </table>
            </td>
        </tr>
    </table>
</body>
</html>
